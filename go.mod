module gitlab.com/gertjana/events

go 1.15

require (
	github.com/aws/aws-lambda-go v1.28.0
	github.com/aws/aws-sdk-go v1.42.44
	github.com/icarus-sullivan/mock-lambda v0.0.0-20220115083805-e065469e964a // indirect
	github.com/olekukonko/tablewriter v0.0.5
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v2 v2.2.8
)
