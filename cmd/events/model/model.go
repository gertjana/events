package model

type Item struct {
	User  string   `json:"user,omitempty"`
	When  string   `json:"when"`
	Title string   `json:"title"`
	Tags  []string `json:"tags"`
}
