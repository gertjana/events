.PHONY: build clean deploy
# requires AWS_PROFILE and ENV as environment variables

build: client
	@env GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o .build/bin/create cmd/events/create/create.go
	@env GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o .build/bin/list cmd/events/list/list.go
	@env GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o .build/bin/search_tag cmd/events/tags/search_tag.go

client:
	@go build -ldflags="-s -w" -o .build/bin/client internal/client/main.go	

clean:
	@rm -rf ./.build ./vendor Gopkg.lock

package:
	@ssocred ${AWS_PROFILE}
	@AWS_PROFILE=${AWS_PROFILE} ENV=${ENV} sls package

deploy: clean build
	@ssocred ${AWS_PROFILE}
	@AWS_PROFILE=${AWS_PROFILE} ENV=${ENV} sls deploy --verbose

config:
	@AWS_PROFILE=${AWS_PROFILE} ENV=${ENV} ./update_config.sh 

domain:
	@ssocred ${AWS_PROFILE}
	@AWS_PROFILE=${AWS_PROFILE} ENV=${ENV} sls create_domain --verbose

test: 
	@go test -v internal/client/impl/client*

format: 
	@gofmt -w cmd/*
