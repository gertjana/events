package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig(t *testing.T) {
	const placeholder = "after build/deploy use make config to fill this"
	cfg, err := LoadFromFile("../../../config_template.yml")
	assert.NoError(t, err)

	assert.Equal(t, placeholder, cfg.Backend.Endpoint)
	assert.Equal(t, placeholder, cfg.Backend.Apikey)
	assert.Equal(t, placeholder, cfg.User)
}
