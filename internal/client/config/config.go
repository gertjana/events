package config

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Backend struct {
		Endpoint string `yaml:"endpoint"`
		Apikey   string `yaml:"apikey"`
	}
	User string `yaml:"user"`
}

const DEFAULT_CONFIG = "config.yml"

func Load() (Config, error) {
	return LoadFromFile(DEFAULT_CONFIG)
}

func LoadFromFile(file string) (Config, error) {
	f, err := os.Open(file)
	if err != nil {
		return Config{}, err
	}
	defer f.Close()

	var cfg Config
	err = yaml.NewDecoder(f).Decode(&cfg)
	if err != nil {
		return Config{}, err
	}
	return cfg, nil
}
