package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/olekukonko/tablewriter"
	"gitlab.com/gertjana/events/cmd/events/model"
	"gitlab.com/gertjana/events/internal/client/config"
	client "gitlab.com/gertjana/events/internal/client/impl"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("syntax: \nclient add <title> <tags..> (tags start with a +) \nclient list\nclient tag <tag>")
		os.Exit(1)
	}
	cfg, err := config.Load()
	exitOnErr(err, 0)

	items, status, err := client.ParseAndExecute(os.Args[1:], cfg)
	exitOnErr(err, status)

	print_list(items)

	os.Exit(0)
}

func exitOnErr(err error, status int) {
	if err != nil {
		fmt.Printf("Error occured: %v, status: %d", err.Error(), status)
		os.Exit(1)
	}
}

func print_list(items []model.Item) {
	if len(items) == 0 {
		fmt.Println("No events found")
		return
	}
	header := []string{"When", "Title", "Tags"}
	rows := make([][]string, len(items))
	for i, item := range items {
		rows[i] = []string{item.When, item.Title, strings.Join(item.Tags, " ")}
	}
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader(header)
	for _, v := range rows {
		table.Append(v)
	}
	table.Render()
}
