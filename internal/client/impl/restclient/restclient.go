package restclient

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

var (
	Client HTTPClient
)

func init() {
	Client = &http.Client{}
}

func Do(method string, url string, body *bytes.Buffer, apikey string) ([]byte, int, error) {
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, 0, err
	}
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")
	request.Header.Set("Accept", "application/json; charset=UTF-8")
	request.Header.Set("x-api-key", apikey)

	response, err := Client.Do(request)
	if err != nil {
		return nil, response.StatusCode, err
	}

	respBody, error := ioutil.ReadAll(response.Body)
	if error != nil {
		return nil, response.StatusCode, err
	}
	defer response.Body.Close()
	return respBody, response.StatusCode, nil
}
