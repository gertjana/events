package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/gertjana/events/cmd/events/model"
	"gitlab.com/gertjana/events/internal/client/config"
	"gitlab.com/gertjana/events/internal/client/impl/restclient"
)

func ParseAndExecute(args []string, cfg config.Config) ([]model.Item, int, error) {

	action := args[0]

	var err error
	var status int
	var items []model.Item
	switch action {
	case "add":
		if len(args) <= 1 {
			return []model.Item{}, 0, errors.New("add needs at least a title")
		}
		event_name, tags := parseTitleAndTags(args[1:])
		items, status, err = add(event_name, tags, cfg)
	case "list":
		items, status, err = list(cfg, "")
	case "tag":
		if len(args) <= 1 {
			return []model.Item{}, 0, errors.New("tag needs a tag to search on")
		}
		tag := args[1]
		items, status, err = list(cfg, tag)
	}

	return items, status, err
}

func parseTitleAndTags(args []string) (string, []string) {

	var title = []string{}
	var tags = []string{}
	for _, arg := range args {
		if arg[0] == '+' {
			tags = append(tags, arg[1:])
		} else {
			title = append(title, arg)
		}
	}
	return strings.Join(title, " "), tags
}

func add(event_name string, tags []string, cfg config.Config) ([]model.Item, int, error) {

	item := model.Item{
		User:  cfg.User,
		When:  time.Now().Format(time.RFC3339),
		Title: event_name,
		Tags:  tags,
	}

	item_json, _ := json.Marshal(item)

	resp, status, err := restclient.Do("POST", cfg.Backend.Endpoint, bytes.NewBuffer(item_json), cfg.Backend.Apikey)
	if err != nil {
		return []model.Item{}, status, err
	}
	returned_item := model.Item{}
	err = json.Unmarshal(resp, &returned_item)
	return []model.Item{returned_item}, status, err

}

func list(cfg config.Config, tag string) ([]model.Item, int, error) {
	url := fmt.Sprintf("%s/%s", cfg.Backend.Endpoint, cfg.User)
	if tag != "" {
		url = fmt.Sprintf("%s/%s/%s", cfg.Backend.Endpoint, cfg.User, tag)
	}

	resp, status, err := restclient.Do("GET", url, bytes.NewBuffer([]byte{}), cfg.Backend.Apikey)
	if err != nil {
		return []model.Item{}, status, err
	}

	items := []model.Item{}
	err = json.Unmarshal(resp, &items)

	return items, status, err
}
