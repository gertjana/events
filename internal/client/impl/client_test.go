package client

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gertjana/events/cmd/events/model"
	"gitlab.com/gertjana/events/internal/client/config"
	"gitlab.com/gertjana/events/internal/client/impl/mocks"
	"gitlab.com/gertjana/events/internal/client/impl/restclient"
)

type ClientTest struct {
	name             string
	args             []string
	expected_results []model.Item
	error_message    string
}

func TestClient(t *testing.T) {
	var clientTests = []ClientTest{
		{
			"add no name or tags",
			[]string{"add"},
			[]model.Item{},
			"add needs at least a title",
		},
		{
			"add no tags",
			[]string{"add", "Test", "Title"},
			[]model.Item{
				{User: "testUser", Title: "Test Title"},
			},
			"",
		},
		{
			"add with tags",
			[]string{"add", "Test", "Title", "+test", "+tag"},
			[]model.Item{
				{User: "testUser", Title: "Test Title", Tags: []string{"test", "tag"}},
			},
			"",
		},
		{
			"add with tags mixed with text",
			[]string{"add", "Test", "+tag", "Title", "+test"},
			[]model.Item{
				{User: "testUser", Title: "Test Title", Tags: []string{"tag", "test"}},
			},
			"",
		},
		{
			"list",
			[]string{"list"},
			[]model.Item{
				{User: "testUser", Title: "Test Title", Tags: []string{"test", "tag"}},
				{User: "testUser", Title: "Test Title", Tags: []string{"test", "tag"}},
			},
			"",
		},
		{
			"tag with no tag",
			[]string{"tag"},
			[]model.Item{},
			"tag needs a tag to search on",
		},
		{
			"tag with tag",
			[]string{"tag", "test"},
			[]model.Item{
				{User: "testUser", Title: "Test Title", Tags: []string{"test", "tag"}},
				{User: "testUser", Title: "Test Title", Tags: []string{"test", "tag"}},
			},
			"",
		},
	}

	restclient.Client = &mocks.MockClient{}

	mocks.GetDoFunc = func(request *http.Request) (*http.Response, error) {
		var r io.ReadCloser
		if request.Method == http.MethodPost {
			r = ioutil.NopCloser(request.Body)
		} else {
			list_result_json := `[
				{"user": "testUser", "title": "Test Title", "when": "2022-22-02T20:22:20.220+01:00", "tags": ["test", "tag"]},
				{"user": "testUser", "title": "Test Title", "when": "2022-22-02T20:22:20.220+01:00", "tags": ["test", "tag"]}
			]`
			r = ioutil.NopCloser(bytes.NewReader([]byte(list_result_json)))
		}
		return &http.Response{
			StatusCode: 200,
			Body:       r,
		}, nil
	}

	testConfig := &config.Config{
		User: "testUser",
	}
	testConfig.Backend.Endpoint = ""
	testConfig.Backend.Apikey = ""

	for _, ct := range clientTests {
		fmt.Printf("Testing: %s\n", ct.name)
		results, _, err := ParseAndExecute(ct.args, *testConfig)
		if err != nil {
			assert.EqualError(t, err, ct.error_message)
			assert.Equal(t, 0, len(results))
		} else {
			assert.Equal(t, len(ct.expected_results), len(results))
			assert.Equal(t, ct.expected_results[0].User, results[0].User)
			assert.Equal(t, ct.expected_results[0].Title, results[0].Title)
			if len(ct.expected_results[0].Tags) != 0 {
				assert.Equal(t, ct.expected_results[0].Tags, results[0].Tags)
			}
		}
	}
}
