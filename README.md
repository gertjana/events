
# Build & Deploy

## Requirements
 - go
 - make
 - ssocred (optional as serverless can't handle aws sso based profiles) 
 - yq: (yaml version of jq)


## Environment variables
| Env var | Description |
|--------:|:------------|
| AWS_PROFILE | The profile that is allowed to deploy this stack to AWS |
| USER | (Optional) when making the config fills in this value for the user |

## Deploy
```
make deploy
```
Executes the serverless.yml, compiles the lambda code and deploy it all to AWS

## Client

```
make client
```
compiles the client code 

```
make config
```
Creates a config for the client and populates it by getting the apigateway endpoint from the stack output and the generated api key from aws

```
make test
```
Runs some test (client only currently)


