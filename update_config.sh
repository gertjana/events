#!/bin/bash
# This requires the following tools to be  installed
# - aws cli
# - ssocred
# - yq

DEFAULT_USER='default'
USER=${USER:=$DEFAULT_USER}
ssocred ${AWS_PROFILE}

# get the generated apikey from aws
APIKEY_NAME=events-apikey
APIKEY_ID=`AWS_PROFILE=${AWS_PROFILE} aws apigateway get-api-keys --output text --query "items[?name=='${APIKEY_NAME}'].id"`
APIKEY_VALUE=`AWS_PROFILE=${AWS_PROFILE} aws apigateway get-api-key --output text --api-key ${APIKEY_ID} --include-value --query "value"`

# get the domain from the stack output
DOMAIN_NAME=`yq eval .DomainName .build/stack.yml`

echo $USER
cp config_template.yml config.yml
yq eval -i '.backend.endpoint |= "https://'$DOMAIN_NAME'/events"' config.yml
yq eval -i '.backend.apikey |= "'$APIKEY_VALUE'"' config.yml
yq eval -i '.user |="'${USER}'"' config.yml

